Задание выполнил студент 321 группы Пантюхин Лев.  
  
**Сборка и запуск:**  
  
*mkdir build*  
*cd build*  
*cmake ..*  
*make*  
*./main*  
  
  
При выполнении использован шаблон.   
Для освещения использована локальная модель освещения Фонга.  
Сцена состоит из 2 источников света и 6 примитивов.  
  
**Баллы:**  
    
**-------------------------------**     
Базовая часть             | 15  
Резкие тени               | 1  
Отражения                 | 1  
Туман                     | 1  
Реалистичность            | 1-2  
Интерактивное перемещение | 2  
**-------------------------------**  
    
**Управление:**   
  
W/A/S/D - прямо/назад/влево/вправо  
R/F - вверх/вниз  
Left shift - ускорение движения  
Mouse - поворот камеры  
Scroll - изменение угла обзора (от 45 до 90 градусов)  
0 - сцена по умолчанию (отражения отключены, поэтому зеркальная поверхность имеет черный цвет)  
1 - включение движения  
2 - включение отражений (по умолчанию глубина 3)  
3 - включение тумана  