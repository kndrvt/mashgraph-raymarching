
#version 330

#define float2 vec2
#define float3 vec3
#define float4 vec4
#define float4x4 mat4
#define float3x3 mat3

in float2 fragmentTexCoord;
layout(location = 0) out vec4 fragColor;

uniform bool REFLECTION;
uniform bool FOG;
uniform float time;
uniform float fov;
uniform mat4 camViewMatrix;
uniform vec3 camPos;
uniform int screenWidth;
uniform int screenHeight;

struct SurfaceMaterials {
  vec4 a;
  vec4 d;
  vec4 s;
  float shininess;
  float reflection;
};

struct Objects {
  int type;
  vec3 center;
  vec3 options;
  SurfaceMaterials material;
};

struct Lights {
  vec3 point;
  float intensive;
};

const float pi = 3.1415926535;
// const float fov = pi / 4.0;
const float eps = 0.001;
const int max_marching_steps = 512;
const float max_marching_dist = 1500.0;
float depth_reflection = REFLECTION ? 3 : 1;
const float speed = 1;
const float fog_min_dist = 1.0;
const float fog_max_dist = 500;
const vec4 fog_color = vec4(0.5, 0.5, 0.5, 1.0);


const int ocount = 6;
const int lcount = 2;

const float fi1 = - pi / 2.0;
const float fi2 = pi / 2.0;
const float fi3 = 0.0;

// initialisation of materials
const SurfaceMaterials obsidian = SurfaceMaterials(
    vec4(0.05375, 0.05, 0.06625, 0.82), vec4(0.18275, 0.17, 0.22525, 0.82),
    vec4(0.332741, 0.328634, 0.346435, 0.82), 38.4, 0.0);

const SurfaceMaterials chrome =SurfaceMaterials(
    vec4(0.25, 0.25, 0.25, 1.0), vec4(0.4, 0.4, 0.4, 1.0),
    vec4(0.774597, 0.774597, 0.774597, 1.0), 76.8, 0.1);

const SurfaceMaterials gold = SurfaceMaterials(
    vec4(0.24725, 0.1995, 0.0745, 1.0), vec4(0.75164, 0.60648, 0.22648, 1.0),
    vec4(0.628281, 0.555802, 0.366065, 1.0), 51.2, 0.1);

const SurfaceMaterials jade = SurfaceMaterials(
    vec4(0.135, 0.2225, 0.1575, 0.95), vec4(0.54, 0.89, 0.63, 0.95),
    vec4(0.316228, 0.316228, 0.316228, 0.95), 12.8, 0.0);

const SurfaceMaterials emerald = SurfaceMaterials(
    vec4(0.0215, 0.1745, 0.0215, 0.55), vec4(0.07568, 0.61424, 0.07568, 0.55),
    vec4(0.633, 0.727811, 0.633, 0.55), 76.8, 0.0);

const SurfaceMaterials mirror = SurfaceMaterials(
    vec4(0.0, 0.0, 0.0, 0.0), vec4(0.0, 0.0, 0.0, 0.0),
    vec4(0.0, 0.0, 0.0, 0.0), 1.0, 1.0);    

float w = float(screenWidth);
float h = float(screenHeight);
float r = 50.0;
vec3 offset1 =
    vec3(0.0, 2 * r * cos(fi1 + speed * time), 2 * r * sin(fi1 + speed * time));
vec3 offset2 =
    vec3(r * cos(fi2 + speed * time), 0.0, r * sin(fi2 + speed * time));
vec3 offset3 =
    vec3(0.0, r * cos(fi3 + speed * time), r * sin(fi3 + speed * time));

Lights lights[lcount];
Objects scene_objects[ocount] = Objects[ocount](
Objects(0, vec3(0.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0) , emerald),
Objects(1, vec3(w, h / 2.0, 50.0) + offset1, vec3(20.0, 0.0, 0.0), chrome),
Objects(2, vec3(w, h / 2.0, 50.0) + offset1 + offset2, vec3(15.0, 15.0, 15.0), gold),
Objects(2, vec3(w, h / 2.0, 50.0) + offset1 - offset2, vec3(15.0, 15.0, 15.0), jade),
Objects(3, vec3(w, h / 2.0, 50.0) + offset1 + offset3, vec3(10.0, 10.0, 0.0), mirror),
Objects(3, vec3(w, h / 2.0, 50.0) + offset1 - offset3, vec3(10.0, 10.0, 0.0), obsidian));

vec3 EyeRayDir(float x, float y, vec3 eye_pos) {
  vec3 ray_dir;
  ray_dir = vec3(x - w / 2.0, y - h / 2.0, -w / (2 * tan(fov / 2.0)));
  return normalize(ray_dir);
}

//-----------------------------------------------------------------------------------------------------
// PRIMITIVES, SCENE, DISTANSE
//-----------------------------------------------------------------------------------------------------

float sdSphere(vec3 p, vec3 c, float r) {
  return length(p - c) - r;
}

float udBox(vec3 p, vec3 c, vec3 b) {
  p -= c;
  return length(max(abs(p) - b, 0.0));
}

float sdTorus(vec3 p, vec3 c, vec2 t) {
  p -= c;
  vec2 q = vec2(length(p.xz) - t.x, p.y);
  return length(q) - t.y;
}

float sdPlane(vec3 p, vec3 n) {
  return dot(p, n.xyz);
}

float sdScene(vec3 point) {
  int index = -1;
  float min_dist = max_marching_dist;
  float dist;
  for (int i = 0; i < ocount; ++i) {
    if (scene_objects[i].type == 0) {
      dist = sdPlane(point, scene_objects[i].options);
    } else if (scene_objects[i].type == 1) {
      dist = sdSphere(point, scene_objects[i].center, scene_objects[i].options.x);
    } else if (scene_objects[i].type == 2) {
      dist = udBox(point, scene_objects[i].center, scene_objects[i].options);
    } else if (scene_objects[i].type == 3) {
      dist = sdTorus(point, scene_objects[i].center, scene_objects[i].options.xy);
    } 
    if (min_dist > dist) {
      index = i;
      min_dist = dist;
    }
  }
  return min_dist;
}

float RayMarchingToObject(vec3 eye, vec3 dir) {
  float cur_dist = 100 * eps;
  for (int i = 0; i < max_marching_steps; i++) {
    float left_dist = sdScene(eye + cur_dist * dir);
    if (left_dist < eps) {
      return cur_dist;
    }
    cur_dist += left_dist;
    if (cur_dist >= max_marching_dist) {
      return max_marching_dist;
    }
  }
  return max_marching_dist;
}

int idObject(vec3 point) {
  float dist;
  for (int i = 0; i < ocount; ++i) {
    if (scene_objects[i].type == 0) {
      dist = sdPlane(point, scene_objects[i].options);
    } else if (scene_objects[i].type == 1) {
      dist = sdSphere(point, scene_objects[i].center, scene_objects[i].options.x);
    } else if (scene_objects[i].type == 2) {
      dist = udBox(point, scene_objects[i].center, scene_objects[i].options);
    } else if (scene_objects[i].type == 3) {
      dist = sdTorus(point, scene_objects[i].center, scene_objects[i].options.xy);
    } 
    if (dist < eps) return i;
  }
}

vec4 background(vec3 eye_dir, float x, float y) {
  return vec4(y / (2 * h) + 0.2, y / (2 * h) + 0.3, y / (2 * h) + 0.5, 1.0);
  // return vec4(0.0, 0.0, 0.0, 0.0);
}

//-----------------------------------------------------------------------------------------------------
// PHONG LIGHTING
//-----------------------------------------------------------------------------------------------------

vec3 SurfaceNormal(vec3 point) {
  return normalize(
      vec3(sdScene(vec3(point.x + eps, point.y, point.z)) -
               sdScene(vec3(point.x - eps, point.y, point.z)),
           sdScene(vec3(point.x, point.y + eps, point.z)) -
               sdScene(vec3(point.x, point.y - eps, point.z)),
           sdScene(vec3(point.x, point.y, point.z + eps)) -
               sdScene(vec3(point.x, point.y, point.z - eps))));
}

vec4 PhongLighting(vec3 point, Lights light_source, vec3 eye_pos, SurfaceMaterials material, int flag) {
  vec3 n = SurfaceNormal(point);
  vec3 h = normalize(eye_pos - point);
  vec3 l = normalize(light_source.point - point);
  vec3 r = normalize(reflect(-l, n));
  return material.a +
         light_source.intensive *
             (material.d * max(dot(l, n), 0.0) +
              material.s * pow(max(dot(r, h), 0.0), material.shininess)) * flag;
}

bool RayMarchingToLight(vec3 point, vec3 light) {
  float cur_dist = 100 * eps;
  vec3 dir = normalize(light - point);
  // if (dot(dir, SurfaceNormal(point)) < 0.0) {
  //   return true;
  // }
  for (int i = 0; i < max_marching_steps; i++) {
    float left_dist= sdScene(point + cur_dist * dir);
    if (left_dist < eps) {
      return true;
    }
    cur_dist += left_dist;
    if (length(light - point) - cur_dist < eps) {
      return false;
    }
  }
  return false;
}

//-----------------------------------------------------------------------------------------------------
// MAIN
//-----------------------------------------------------------------------------------------------------

void main(void) {
  
  // get curr pixelcoordinates
  float x = fragmentTexCoord.x * w;
  float y = fragmentTexCoord.y * h;

  // generate initial ray
  vec3 eye_pos = camPos;
  vec3 eye_dir = EyeRayDir(x, y, eye_pos);
  eye_dir = (camViewMatrix * vec4(eye_dir, 1.0)).xyz;

  lights[0] = Lights(vec3(2 * w / 3, 3 * h / 4, 0.0), 1.0);
  lights[1] = Lights(vec3(w, 3 * h / 4, 200.0), 1.0);
  vec3 point = vec3(0.0, 0.0, 0.0);
  vec4 color = vec4(0.0, 0.0, 0.0, 0.0);
  float k = 1.0;
  for (int i = 0; i < depth_reflection; ++i) {
    
    float dist = RayMarchingToObject(eye_pos, eye_dir);
    if (dist > max_marching_dist - eps) {
      color += k * background(eye_pos, x, y);
      break;
    }
    point = eye_pos + dist * eye_dir;
    int id = idObject(point);
    k -= scene_objects[id].material.reflection;
    if (k < 0.0) break;
    for (int i = 0; i < lcount; ++i) {
      if (!RayMarchingToLight(point, lights[i].point)) {
        color += k * PhongLighting(point, lights[i], eye_pos, scene_objects[id].material, 1);
      } else {
        color += k * PhongLighting(point, lights[i], eye_pos, scene_objects[id].material, 0);
      }
    }
    if (scene_objects[id].material.reflection <= 0.0) {
      break;
    }
    k = scene_objects[id].material.reflection;
    eye_dir = reflect(eye_dir, SurfaceNormal(point));
    eye_pos = point;
  }
  // fog
  //
  if (FOG) {
    float m = clamp((fog_max_dist - length(camPos - point)) / (fog_max_dist - fog_min_dist), 0.0, 1.0);
    color = mix(fog_color, color, m);
  }
  fragColor = min(vec4(1.0, 1.0, 1.0, 1.0), color);
}