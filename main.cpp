// internal includes
#include "common.h"
#include "ShaderProgram.h"
#include "LiteMath.h"

// External dependencies
// #define GLFW_DLL
#include <GLFW/glfw3.h>
#include <random>

static GLsizei WIDTH = 1024, HEIGHT = 640;  //размеры окна

using namespace LiteMath;

const float pi = 3.1415926535;

bool keys[1024];
float lastX = 0.0, lastY = 0.0;
float yaw = 0.0f;
float pitch = 0.0f;
bool firstMouse = true;
float fov = pi / 4.0;
float3 cameraPos(WIDTH / 2, HEIGHT / 2, 0.0);
float3 cameraFront(1.0, 0.0, 0.0);
float3 cameraUp(0.0, 1.0, 0.0);
float cameraSpeed = 10;
bool MOVE = false, REFLECTION = false, FOG = false;

float radians(float angle) { return angle * 2 * pi / 360; }

void windowResize(GLFWwindow* window, int width, int height) {
  WIDTH = width;
  HEIGHT = height;
}

static void mouseCallback(GLFWwindow* window, double xpos, double ypos) {
  if (firstMouse) {
    lastX = xpos;
    lastY = ypos;
    firstMouse = false;
  }

  float xoffset = xpos - lastX;
  float yoffset = lastY - ypos;
  lastX = xpos;
  lastY = ypos;

  float sensitivity = 0.05;
  xoffset *= sensitivity;
  yoffset *= sensitivity;

  yaw += xoffset;
  pitch += yoffset;
  if (pitch > 89.0) pitch = 89.0;
  if (pitch < -89.0) pitch = -89.0;

  float3 front;
  front.x = cos(radians(pitch)) * cos(radians(yaw));
  front.y = sin(radians(pitch));
  front.z = cos(radians(pitch)) * sin(radians(yaw));
  cameraFront = normalize(front);
}

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mode) {
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);
  if ((key == GLFW_KEY_0) && (action == GLFW_PRESS)) {
    cameraPos = float3(WIDTH / 2, HEIGHT / 2, 0.0);
    cameraFront = float3(1.0, 0.0, 0.0);
    cameraUp = float3(0.0, 1.0, 0.0);
    MOVE = false;
    REFLECTION = false;
    FOG = false;
    glfwSetTime(0.0);
  }
  if ((key == GLFW_KEY_1) && (action == GLFW_PRESS)) MOVE = !MOVE;
  if ((key == GLFW_KEY_2) && (action == GLFW_PRESS)) REFLECTION = !REFLECTION;
  if ((key == GLFW_KEY_3) && (action == GLFW_PRESS)) FOG = !FOG;
  if (action == GLFW_PRESS)
    keys[key] = true;
  else if (action == GLFW_RELEASE)
    keys[key] = false;
}

void scrollCallback(GLFWwindow* window, double xoffset, double yoffset) {
  if (fov >= pi / 4 && fov <= pi / 2) fov -= yoffset * 0.01;
  if (fov <= pi / 4) fov = pi / 4;
  if (fov >= pi / 2) fov = pi / 2;
}

void move() {
  if (keys[GLFW_KEY_W]) cameraPos += cameraSpeed * cameraFront;
  if (keys[GLFW_KEY_S]) cameraPos -= cameraSpeed * cameraFront;
  if (keys[GLFW_KEY_A])
    cameraPos -= normalize(cross(cameraFront, cameraUp)) * cameraSpeed;
  if (keys[GLFW_KEY_D])
    cameraPos += normalize(cross(cameraFront, cameraUp)) * cameraSpeed;
  if (keys[GLFW_KEY_R]) cameraPos += cameraSpeed * cameraUp;
  if (keys[GLFW_KEY_F]) cameraPos -= cameraSpeed * cameraUp;
  if (keys[GLFW_KEY_LEFT_SHIFT] && cameraSpeed < 20) cameraSpeed *= 2;
  if (!keys[GLFW_KEY_LEFT_SHIFT] && cameraSpeed > 10) cameraSpeed /= 2;
}

int initGL() {
  int res = 0;
  //грузим функции opengl через glad
  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
    std::cout << "Failed to initialize OpenGL context" << std::endl;
    return -1;
  }

  std::cout << "Vendor: " << glGetString(GL_VENDOR) << std::endl;
  std::cout << "Renderer: " << glGetString(GL_RENDERER) << std::endl;
  std::cout << "Version: " << glGetString(GL_VERSION) << std::endl;
  std::cout << "GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION)
            << std::endl;

  return 0;
}

int main(int argc, char** argv) {
  if (!glfwInit()) return -1;

  //запрашиваем контекст opengl версии 3.3
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

  GLFWwindow* window =
      glfwCreateWindow(WIDTH, HEIGHT, "Lol kek cheburek", nullptr, nullptr);
  if (window == nullptr) {
    std::cout << "Failed to create GLFW window" << std::endl;
    glfwTerminate();
    return -1;
  }

  glfwMakeContextCurrent(window);
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  glfwSetCursorPosCallback(window, mouseCallback);
  glfwSetKeyCallback(window, keyCallback);
  glfwSetScrollCallback(window, scrollCallback);
  glfwSetWindowSizeCallback(window, windowResize);
  glfwSetCursorPos(window, WIDTH / 2, HEIGHT / 2);

  if (initGL() != 0) return -1;

  // Reset any OpenGL errors which could be present for some reason
  GLenum gl_error = glGetError();
  while (gl_error != GL_NO_ERROR) gl_error = glGetError();

  //создание шейдерной программы из двух файлов с исходниками шейдеров
  //используется класс-обертка ShaderProgram
  std::unordered_map<GLenum, std::string> shaders;
  shaders[GL_VERTEX_SHADER] = "../shaders/vertex.glsl";
  shaders[GL_FRAGMENT_SHADER] = "../shaders/fragment.glsl";
  ShaderProgram program(shaders);
  GL_CHECK_ERRORS;

  glfwSwapInterval(1);  // force 60 frames per second

  //Создаем и загружаем геометрию поверхности
  //
  GLuint g_vertexBufferObject;
  GLuint g_vertexArrayObject;
  {
    float quadPos[] = {
        -1.0f, 1.0f,   // v0 - top left corner
        -1.0f, -1.0f,  // v1 - bottom left corner
        1.0f, 1.0f,    // v2 - top right corner
        1.0f, -1.0f    // v3 - bottom right corner
    };

    g_vertexBufferObject = 0;
    GLuint vertexLocation =
        0;  // simple layout, assume have only positions at location = 0

    glGenBuffers(1, &g_vertexBufferObject);
    GL_CHECK_ERRORS;
    glBindBuffer(GL_ARRAY_BUFFER, g_vertexBufferObject);
    GL_CHECK_ERRORS;
    glBufferData(GL_ARRAY_BUFFER, 4 * 2 * sizeof(GLfloat), (GLfloat*)quadPos,
                 GL_STATIC_DRAW);
    GL_CHECK_ERRORS;

    glGenVertexArrays(1, &g_vertexArrayObject);
    GL_CHECK_ERRORS;
    glBindVertexArray(g_vertexArrayObject);
    GL_CHECK_ERRORS;

    glBindBuffer(GL_ARRAY_BUFFER, g_vertexBufferObject);
    GL_CHECK_ERRORS;
    glEnableVertexAttribArray(vertexLocation);
    GL_CHECK_ERRORS;
    glVertexAttribPointer(vertexLocation, 2, GL_FLOAT, GL_FALSE, 0, 0);
    GL_CHECK_ERRORS;

    glBindVertexArray(0);
  }

  //цикл обработки сообщений и отрисовки сцены каждый кадр
  float time = 0.0;
  while (!glfwWindowShouldClose(window)) {
    glfwPollEvents();
    if (!MOVE) {
      glfwSetTime(time);
    }
    time = glfwGetTime();
    move();

    //очищаем экран каждый кадр
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    GL_CHECK_ERRORS;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    GL_CHECK_ERRORS;

    program.StartUseShader();
    GL_CHECK_ERRORS;

    float4x4 camViewMatrix = transpose4x4(lookAtTransposed(cameraPos, cameraPos + cameraFront, cameraUp));
    
    program.SetUniform("REFLECTION", REFLECTION);
    program.SetUniform("FOG", FOG);
    program.SetUniform("fov", fov);
    program.SetUniform("time", time);
    program.SetUniform("camViewMatrix", camViewMatrix);
    program.SetUniform("camPos", cameraPos);
    program.SetUniform("screenWidth", WIDTH);
    program.SetUniform("screenHeight", HEIGHT);

    // очистка и заполнение экрана цветом
    //
    glViewport(0, 0, WIDTH, HEIGHT);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    // draw call
    //
    glBindVertexArray(g_vertexArrayObject);
    GL_CHECK_ERRORS;
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    GL_CHECK_ERRORS;  // The last parameter of glDrawArrays is equal to VS
                      // invocations

    program.StopUseShader();

    glfwSwapBuffers(window);
  }

  //очищаем vboи vao перед закрытием программы
  //
  glDeleteVertexArrays(1, &g_vertexArrayObject);
  glDeleteBuffers(1, &g_vertexBufferObject);

  glfwTerminate();
  return 0;
}
